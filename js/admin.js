"use strict";
// Import the functions you need from the SDKs you need
import { onValue, ref, set, child, get, update } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { ref as refStorage, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
import { signOut } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
import { db, storage, auth } from "./config.js";

// Declarar elementos del DOM
const resultados = document.querySelector("#resultados");
const imagen = document.querySelector("#imagen");

// Variables input
const obtenerCampos = () => {
	event.preventDefault();

	return {
		codigo: document.querySelector("#codigo").value.trim(),
		nombre: document.querySelector("#nombre").value.trim(),
		precio: document.querySelector("#precio").value.trim(),
		descripcion: document.querySelector("#descripcion").value.trim(),
	};
};

const limpiarCampos = () => {
	event.preventDefault();
	document.querySelector("#codigo").value = "";
	document.querySelector("#nombre").value = "";
	document.querySelector("#precio").value = "";
	document.querySelector("#descripcion").value = "";
	document.querySelector("#url").value = "";
	document.querySelector("#vistaPrevia").classList.add("d-none");
	resultados.classList.add("d-none");
	resultados.innerHTML = "";
	imagen.value = "";
};

const llenarCampos = ({ codigo, nombre, descripcion, precio, url }) => {
	document.querySelector("#codigo").value = codigo;
	document.querySelector("#nombre").value = nombre;
	document.querySelector("#descripcion").value = descripcion;
	document.querySelector("#precio").value = precio.slice(1);
	document.querySelector("#url").value = url;
	document.querySelector("#vistaPrevia").src = url;
	document.querySelector("#vistaPrevia").classList.remove("d-none");
};

const cambiarImagen = () => {
	document.querySelector("#vistaPrevia").src = URL.createObjectURL(imagen.files[0]);
	document.querySelector("#vistaPrevia").classList.remove("d-none");
};

async function insertarProducto() {
	try {
		event.preventDefault();

		let { codigo, nombre, descripcion, precio } = obtenerCampos();
		const dbref = ref(db);
		const storageRef = refStorage(storage, "productos/" + codigo);

		if (isNaN(parseFloat(precio)) || parseFloat(precio) <= 0) {
			return alert("Escriba un precio válido");
		}

		if (!codigo || !nombre || !descripcion || !imagen.value) {
			return alert("Rellene los campos faltantes");
		}

		const snapshot = await get(child(dbref, "productos/" + codigo));
		if (snapshot.exists()) return alert("Ya existe un producto con ese código");

		await uploadBytes(storageRef, imagen.files[0]);

		let url = await getDownloadURL(storageRef);

		await set(ref(db, "productos/" + codigo), {
			nombre,
			descripcion,
			precio: "$" + precio,
			url,
			status: "0",
		});

		alert("Se insertaron con exito los datos");
	} catch (error) {
		if (error.code === "PERMISSION_DENIED" || error.code === "storage/unauthorized") {
			alert("No cuentas con permisos");
		} else {
			console.error(error);
		}
	}
}

async function buscarProducto() {
	try {
		event.preventDefault();

		let { codigo } = obtenerCampos();
		if (codigo === "") return alert("Escriba un código");

		const dbref = ref(db);
		const snapshot = await get(child(dbref, "productos/" + codigo));

		if (snapshot.exists()) {
			let nombre = snapshot.val().nombre;
			let descripcion = snapshot.val().descripcion;
			let precio = snapshot.val().precio;
			let url = snapshot.val().url;

			document.querySelector("#imagen").value = "";

			llenarCampos({
				codigo,
				nombre,
				descripcion,
				precio,
				url,
			});
		} else {
			alert("No se encontró ese producto");
		}
	} catch (error) {
		if (error.code === "PERMISSION_DENIED") {
			alert("No cuentas con permisos");
		} else {
			console.error(error);
		}
	}
}

async function mostrarProductos() {
	event.preventDefault();

	try {
		const dbref = ref(db, "productos");

		await onValue(dbref, snapshot => {
			resultados.innerHTML = `<thead><tr>
					<th scope="col" width="5%">Código</th>
					<th scope="col" width="25%">Nombre</th>
					<th scope="col" width="40%">Descripción</th>
					<th scope="col" width="15%">Precio</th>
					<th scope="col" width="10%">Imagen</th>
					<th scope="col" width="5%">Estado</th>
				</tr></thead><tbody></tbody>`;

			snapshot.forEach(childSnapshot => {
				const childKey = childSnapshot.key;
				const childData = childSnapshot.val();

				resultados.lastElementChild.innerHTML += `<tr>
					<th scope="row">${childKey}</th>
					<td>${childData.nombre}</td>
					<td>${childData.descripcion}</td>
					<td>${childData.precio}</td>
					<td class="p-0"><img class="w-100" src="${childData.url}" alt="Imagen de ${childData.nombre}"/></td>
					<td>${childData.status}</td>
				</tr>`;
			});
		});

		resultados.classList.remove("d-none");
	} catch (error) {
		if (error.code === "PERMISSION_DENIED") {
			alert("No cuentas con permisos");
		} else {
			console.error(error);
		}
	}
}

async function actualizarProducto() {
	try {
		event.preventDefault();

		let { codigo, nombre, descripcion, precio } = obtenerCampos();
		const storageRef = refStorage(storage, "productos/" + codigo);

		if (isNaN(parseFloat(precio)) || parseFloat(precio) <= 0) {
			return alert("Escriba un precio válido");
		}

		if (!codigo || !nombre || !descripcion) {
			return alert("Rellene los campos faltantes");
		}

		if (!imagen.value) {
			await update(ref(db, "productos/" + codigo), {
				nombre,
				descripcion,
				precio: "$" + precio,
			});
			return alert("Se realizó una actualización");
		}

		await uploadBytes(storageRef, imagen.files[0]);
		let url = await getDownloadURL(storageRef);

		await update(ref(db, "productos/" + codigo), {
			nombre,
			descripcion,
			precio: "$" + precio,
			url,
		});
		return alert("Se realizó una actualización");
	} catch (error) {
		if (error.code === "PERMISSION_DENIED" || error.code === "storage/unauthorized") {
			alert("No estás autentificado");
		} else {
			console.error(error);
		}
	}
}

async function desactivarProducto() {
	try {
		event.preventDefault();

		let { codigo } = obtenerCampos();
		if (codigo === "") return alert("Introduzca un código");
		const dbref = ref(db);

		const snapshot = await get(child(dbref, "productos/" + codigo));
		if (!snapshot.exists()) {
			return alert("No existe un producto con ese código");
		}

		if (snapshot.val().status === "1") {
			await update(ref(db, "productos/" + codigo), { status: "0" });
			alert("El registro fue activado");
		} else {
			await update(ref(db, "productos/" + codigo), { status: "1" });
			alert("El registro fue desactivado");
		}

		await mostrarProductos();
	} catch (error) {
		if (error.code === "PERMISSION_DENIED") {
			alert("No estás autentificado");
		} else {
			console.error(error);
		}
	}
}

document.querySelector("#btnAgregar").addEventListener("click", insertarProducto);
document.querySelector("#btnConsultar").addEventListener("click", buscarProducto);
document.querySelector("#btnActualizar").addEventListener("click", actualizarProducto);
document.querySelector("#btnDeshabilitar").addEventListener("click", desactivarProducto);
document.querySelector("#btnMostrar").addEventListener("click", mostrarProductos);
document.querySelector("#btnLimpiar").addEventListener("click", limpiarCampos);
imagen.addEventListener("change", cambiarImagen);

document.querySelector("#cerrarSesion").addEventListener("click", async e => {
	e.preventDefault();

	await signOut(auth);
});
