import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getDatabase } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { getStorage } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
import { getAuth, signInWithEmailAndPassword, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";

export const firebaseConfig = {
	apiKey: "AIzaSyD5srzLAwCBz-vYONu1fgQVnsQMIC2yhnA",
	authDomain: "webfinal-owo.firebaseapp.com",
	databaseURL: "https://webfinal-owo-default-rtdb.firebaseio.com",
	projectId: "webfinal-owo",
	storageBucket: "webfinal-owo.appspot.com",
	messagingSenderId: "380604409838",
	appId: "1:380604409838:web:adc3cff2ec77d9add090ff",
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const db = getDatabase();
export const storage = getStorage();
export const auth = getAuth(app);

export async function iniciarSesion(user, pass) {
	try {
		await signInWithEmailAndPassword(auth, user, pass);
	} catch (error) {
		throw new Error(error);
	}
}

onAuthStateChanged(auth, async user => {
	if (user) {
		if (window.location.pathname.includes("/admin/") && !window.location.pathname.includes("/admin/admin")) {
			window.location.href = "/admin/admin.html";
		}
	} else {
		if (window.location.pathname.includes("/admin/admin")) {
			window.location.href = "/admin/login.html";
		}
	}
});
